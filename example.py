#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Example."""


def test() -> str:
    """Test.

    Returns:
        str:
            Example text.

    """
    return "Test4."


print(test())
